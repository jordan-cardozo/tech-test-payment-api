﻿using Application.Interfaces;
using Application.Models;
using Domain.Entities;
using Domain.Services;

namespace Application.Apps
{
    public class ApplicationVenda : IAppVenda
    {
        private readonly VendaService _VendaService;
        private readonly IAdapterVenda _VendaAdapter;

        public ApplicationVenda(VendaService VendaService, IAdapterVenda vendaAdapter)
        {
            _VendaService = VendaService;
            _VendaAdapter = vendaAdapter;
        }

        public VendaViewModel Adicionar(VendaViewModel VendaModel)
        {
            var venda = _VendaAdapter.VendaViewModelToEntity(VendaModel, _VendaService);

            _VendaService.Adicionar(venda);

            return _VendaAdapter.VendaEntityToViewModel(venda, _VendaService);
        }

        public VendaViewModel Atualizar(int id, VendaUpdateViewModel VendaModel)
        {
            string status = VendaModel.Status;

            var venda = _VendaService.Atualizar(id, status);

            return _VendaAdapter.VendaEntityToViewModel(venda, _VendaService);
        }

        public VendaViewModel ObterPorId(int id)
        {
            var vendaBanco = _VendaService.ObterPorId(id);

            return _VendaAdapter.VendaEntityToViewModel(vendaBanco, _VendaService);

        }
    }
}

﻿using Domain.Entities;
using Domain.Interfaces.Vendas;

namespace Domain.Services
{
    /// <summary>
    /// Serviço para Venda
    /// </summary>
    public class VendaService
    {
        private readonly InterfaceVenda _InterfaceVenda;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="InterfaceVenda"></param>
        public VendaService(InterfaceVenda InterfaceVenda)
        {
            _InterfaceVenda = InterfaceVenda;
        }

        /// <summary>
        /// Adiciona a Venda e os Itens, associando ao Vendedor caso já exista 
        /// ou criando o registro do Vendedor se não existir.
        /// </summary>
        /// <param name="VendaView"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public Venda Adicionar(Venda VendaView)
        {
            if(VendaView.Status == "Cancelada")
                throw new ArgumentException("Não pode registrar uma venda com Status 'Cancelada'!");


            if (VendaView.Vendedor != null && VendaView.Itens != null && VendaView.Itens.Any())
            {
                _InterfaceVenda.Adicionar(VendaView);

                return VendaView;
            }
            else
            {
                throw new ArgumentException("Informe pelo menos um Item!");
            }
        }

        /// <summary>
        /// Atualiza o Status da Venda
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public Venda Atualizar(int id, string status)
        {
            var vendaBanco = _InterfaceVenda.BuscarVendaPorId(id);


            if (vendaBanco.Status == "Aguardando pagamento")
            {

                if (status == "Pagamento Aprovado")
                {
                    vendaBanco.Status = "Pagamento Aprovado";

                }
                else if (status == "Cancelada")
                {
                    vendaBanco.Status = "Cancelada";
                }
                else
                {
                    throw new ArgumentException("Alteração de Status Inválida!");
                }

            }
            else if (vendaBanco.Status == "Pagamento Aprovado")
            {
                if (status == "Enviado para Transportadora")
                {
                    vendaBanco.Status = "Enviado para Transportadora";

                }
                else if (status == "Cancelada")
                {
                    vendaBanco.Status = "Cancelada";
                }
                else
                {
                    throw new ArgumentException("Alteração de Status Inválida!");
                }

            }
            else if (vendaBanco.Status == "Enviado para Transportadora" && status == "Entregue")
            {

                vendaBanco.Status = "Entregue";

            }
            else
            {
                throw new ArgumentException("Alteração de Status Inválida!");
            }

            _InterfaceVenda.Atualizar(vendaBanco);

            return vendaBanco;
        }

        /// <summary>
        /// Busca a Venda pelo id informado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public Venda ObterPorId(int id)
        {
            var vendaBanco = _InterfaceVenda.BuscarVendaPorId(id);


            if (vendaBanco != null)
            {
                return vendaBanco;
            }
            else
            {
                throw new ArgumentException("Não existe venda com o Id informado!");
            }
        }

        public IQueryable<Vendedor> BuscarVendedorPorNome(string nome)
        {
            return _InterfaceVenda.BuscarVendedorPorNome(nome);
        }

        public Vendedor BuscarVendedorPorId(int id)
        {
            return _InterfaceVenda.BuscarVendedorPorId(id);
        }

        public List<Item> BuscarItens(int id)
        {
            return _InterfaceVenda.BuscarItens(id);
        }

    }
}

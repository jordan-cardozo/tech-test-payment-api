﻿namespace Domain.Entities
{
    /// <summary>
    /// Venda realizada
    /// </summary>
    public class Venda
    {
        public int IdVenda { get; set; }
        public string IdPedido { get; set; }
        public int IdVendedor { get; set; }
        public Vendedor Vendedor { get; set; }
        public DateTime Data { get; set; }

        /// <summary>
        /// Propriedades de navegação relacional
        /// </summary>
        public virtual IList<Item> Itens { get; set; }
        public string Status { get; set; }
    }
}

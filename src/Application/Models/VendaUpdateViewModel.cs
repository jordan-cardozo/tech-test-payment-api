﻿using System.ComponentModel.DataAnnotations;

namespace Application.Models
{
    /// <summary>
    /// Modelo de atualização de Venda para apresentação
    /// </summary>
    public class VendaUpdateViewModel
    {
        [Required(ErrorMessage = "Informe o status da venda.", AllowEmptyStrings = false)]
        [MinLength(8, ErrorMessage = "Tamanho mínimo de {0} caracteres.")]
        public string Status { get; set; }
    }
}

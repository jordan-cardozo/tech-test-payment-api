﻿using Domain.Entities;
using Domain.Interfaces.Vendas;
using Infra.Context;

namespace Infra.Repository.Vendas
{
    public class RepositoryVendas : InterfaceVenda
    {
        private readonly DatabaseContext _context;

        public RepositoryVendas(DatabaseContext context)
        {
            _context = context;

        }


        public void Adicionar(Venda Entity)
        {
            _context.Vendas.Add(Entity);
            _context.SaveChanges();
        }

        public void Atualizar(Venda Entity)
        {
            _context.Vendas.Update(Entity);
            _context.SaveChanges();
        }

        public Venda BuscarVendaPorId(int id)
        {
            return _context.Vendas.Find(id);
        }

        public Vendedor BuscarVendedorPorId(int id)
        {
            return _context.Vendedores.Find(id);
        }

        public List<Item> BuscarItens(int id)
        {
            return _context.Itens.Where(x => x.IdVenda.Equals(id)).ToList();
        }
        public IQueryable<Vendedor> BuscarVendedorPorNome(string nome)
        {
            return _context.Vendedores.Where(x => x.Nome.Equals(nome));
        }
    }
}

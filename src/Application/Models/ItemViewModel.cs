﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Application.Models
{
    /// <summary>
    /// Modelo de Item para apresentação
    /// </summary>
    public class ItemViewModel
    {
        [Required(ErrorMessage = "Informe uma descrição.", AllowEmptyStrings = false)]
        [MaxLength(255, ErrorMessage = "Limite máximo de {0} caracteres.")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Informe o preço.")]
        public decimal Preco { get; set; }

        [DefaultValue(1)]
        [Required(ErrorMessage = "Informe a quantidade.")]
        [Range(1, int.MaxValue)]
        public int Quantidade { get; set; }
    }
}

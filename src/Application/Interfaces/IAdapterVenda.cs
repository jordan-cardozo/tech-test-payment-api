﻿using Domain.Entities;
using Application.Models;
using Domain.Services;

namespace Application.Interfaces
{
    public interface IAdapterVenda
    {
        Venda VendaViewModelToEntity(VendaViewModel ViewModel, VendaService VendaService);
        VendaViewModel VendaEntityToViewModel(Venda Entity, VendaService VendaService);
    }
}

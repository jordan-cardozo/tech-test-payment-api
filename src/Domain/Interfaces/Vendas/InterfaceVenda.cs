﻿using Domain.Entities;

namespace Domain.Interfaces.Vendas
{
    public interface InterfaceVenda
    {
        void Adicionar(Venda Entity);
        void Atualizar(Venda Entity);
        Venda BuscarVendaPorId(int id);
        Vendedor BuscarVendedorPorId(int id);
        List<Item> BuscarItens(int id);
        IQueryable<Vendedor> BuscarVendedorPorNome(string nome);
    }
}

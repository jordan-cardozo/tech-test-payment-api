﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infra.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Item> Itens { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Vendedor>(tabela => {
                tabela.HasKey(x => x.IdVendedor);
            });

            builder.Entity<Venda>(tabela => {
                tabela.HasKey(v => v.IdVenda);
            });

            builder.Entity<Item>()
                .HasKey(i => new { i.IdItem });

            builder.Entity<Venda>()
                .HasOne<Vendedor>(v => v.Vendedor)
                .WithMany(x => x.Vendas)
                .HasForeignKey(v => v.IdVendedor);

            builder.Entity<Item>()
                .HasOne<Venda>(i => i.Venda)
                .WithMany(v => v.Itens)
                .HasForeignKey(i => i.IdVenda);

        }
    }
}

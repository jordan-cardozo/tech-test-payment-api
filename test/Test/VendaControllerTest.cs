using Application.Adapter;
using Application.Apps;
using Application.Interfaces;
using Application.Models;
using Domain.Interfaces.Vendas;
using Domain.Services;
using Infra.Context;
using Infra.Repository.Vendas;
using Microsoft.EntityFrameworkCore;

namespace Test
{
    public class VendaControllerTest
    {
        private readonly DatabaseContext _context;
        private readonly InterfaceVenda _ReposVenda;
        private readonly IAppVenda _AppVenda;
        private readonly IAdapterVenda _VendaAdapter;
        private readonly VendaService _VendaService;
        private readonly VendedorViewModel _Vendedor;
        private readonly List<ItemViewModel> _Items;

        public VendaControllerTest()
        {
            var builder = new DbContextOptionsBuilder<DatabaseContext>();
            builder.UseInMemoryDatabase("InMemoryDatabase");
            var options = builder.Options;

            _context = new DatabaseContext(options);
            _ReposVenda = new RepositoryVendas(_context);
            _VendaService = new VendaService(_ReposVenda);
            _VendaAdapter = new VendaAdapter();            
            _AppVenda = new ApplicationVenda(_VendaService, _VendaAdapter);

            _Vendedor = new VendedorViewModel { Cpf = "724.815.846-56", Nome = "Mariellen Costa Gastaldi", Email = "mari_gastaldi@gmail.com", Telefone = "(27)99816-2136" };

            _Items = new List<ItemViewModel>();

            _Items.Add(new ItemViewModel { Descricao = "Gabinete Red Dragon", Preco = 219.90M, Quantidade = 2 });
            _Items.Add(new ItemViewModel { Descricao = "Mem�ria 8GB Kingston", Preco = 319.99M, Quantidade = 5 });
            _Items.Add(new ItemViewModel { Descricao = "Fonte 500W Mymax", Preco = 279.90M, Quantidade = 1 });

            _context.Database.EnsureDeleted();
        }

        [Fact]
        public void GetVendaSucesso()
        {
            VendaViewModel vendaModel = new VendaViewModel { Itens = _Items, Vendedor = _Vendedor, Status = "Aguardando pagamento" };
            _AppVenda.Adicionar(vendaModel);

            var atual = _AppVenda.ObterPorId(1);

            Assert.Equal(vendaModel.Vendedor.Cpf, atual.Vendedor.Cpf);
            Assert.Equal(vendaModel.Vendedor.Nome, atual.Vendedor.Nome);
            Assert.Equal(vendaModel.Vendedor.Email, atual.Vendedor.Email);
            Assert.Equal(vendaModel.Vendedor.Telefone, atual.Vendedor.Telefone);
            Assert.Equal(vendaModel.Status, atual.Status);
            Assert.Equal(3, atual.Itens.Count);
        }

        [Fact]
        public void GetVendaFalha()
        {
            VendaViewModel vendaModel = new VendaViewModel { Itens = _Items, Vendedor = _Vendedor, Status = "Aguardando pagamento" };
            _AppVenda.Adicionar(vendaModel);

            var ex = Assert.Throws<ArgumentException>(() => _AppVenda.ObterPorId(0));

            Assert.Equal("N�o existe venda com o Id informado!", ex.Message);
        }

        [Fact]
        public void PostVendaSucesso()
        {
            _AppVenda.Adicionar(GetFullVendaViewModel());
            var atual = _AppVenda.ObterPorId(1);

            Assert.Equal(_Vendedor.Cpf, atual.Vendedor.Cpf);
            Assert.Equal(_Vendedor.Nome, atual.Vendedor.Nome);
            Assert.Equal(_Vendedor.Email, atual.Vendedor.Email);
            Assert.Equal(_Vendedor.Telefone, atual.Vendedor.Telefone);
            Assert.Equal("Aguardando pagamento", atual.Status);
            Assert.Equal(3, atual.Itens.Count);

        }

        [Fact]
        public void PostVendaFalha()
        {
            VendaViewModel vendaModel = new VendaViewModel { Vendedor = _Vendedor, Status = "Aguardando pagamento" };

            var ex = Assert.Throws<ArgumentException>(() => _AppVenda.Adicionar(vendaModel));

            Assert.Equal("Informe pelo menos um Item!", ex.Message);
        }

        [Fact]
        public void PutVendaSucesso()
        {
            _AppVenda.Adicionar(GetFullVendaViewModel());

            Assert.Equal("Pagamento Aprovado", AtualizarStatus(1, "Pagamento Aprovado").Status);
            Assert.Equal("Enviado para Transportadora", AtualizarStatus(1, "Enviado para Transportadora").Status);
            Assert.Equal("Entregue", AtualizarStatus(1, "Entregue").Status);


            _AppVenda.Adicionar(GetFullVendaViewModel());
            Assert.Equal("Pagamento Aprovado", AtualizarStatus(2, "Pagamento Aprovado").Status);
            Assert.Equal("Cancelada", AtualizarStatus(2, "Cancelada").Status);

            _AppVenda.Adicionar(GetFullVendaViewModel());
            Assert.Equal("Cancelada", AtualizarStatus(3, "Cancelada").Status);

        }

        [Fact]
        public void PutVendaFalha()
        {
            _AppVenda.Adicionar(GetFullVendaViewModel());

            var ex = Assert.Throws<ArgumentException>(() => _AppVenda.Atualizar(1, GetVendaUpdateViewModel("Enviado para Transportadora")));

            Assert.Equal("Altera��o de Status Inv�lida!", ex.Message);



            _AppVenda.Adicionar(GetFullVendaViewModel());

            var ex1 = Assert.Throws<ArgumentException>(() => _AppVenda.Atualizar(2, GetVendaUpdateViewModel("Entregue")));

            Assert.Equal("Altera��o de Status Inv�lida!", ex1.Message);



            _AppVenda.Adicionar(GetFullVendaViewModel());
            AtualizarStatus(2, "Pagamento Aprovado");

            var ex2 = Assert.Throws<ArgumentException>(() => _AppVenda.Atualizar(3, GetVendaUpdateViewModel("Entregue")));

            Assert.Equal("Altera��o de Status Inv�lida!", ex2.Message);
        }

        public VendaViewModel AtualizarStatus(int id, string status)
        {
            VendaUpdateViewModel vendaUpdateViewModel = new VendaUpdateViewModel { Status = status };
            _AppVenda.Atualizar(id, vendaUpdateViewModel);
            return _AppVenda.ObterPorId(id);
        }

        public VendaViewModel GetFullVendaViewModel()
        {

            VendaViewModel vendaModel = new VendaViewModel { Itens = _Items, Vendedor = _Vendedor, Status = "Aguardando pagamento" };

            return vendaModel;
        }

        public VendaUpdateViewModel GetVendaUpdateViewModel(string status)
        {
            VendaUpdateViewModel vendaUpdateViewModel = new VendaUpdateViewModel { Status = status };
            return vendaUpdateViewModel;
        }
    }
}
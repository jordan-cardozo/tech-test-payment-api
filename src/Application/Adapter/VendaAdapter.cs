﻿using Application.Interfaces;
using Application.Models;
using Domain.Entities;
using Domain.Services;

namespace Application.Adapter
{
    public class VendaAdapter : IAdapterVenda
    {

        public Venda VendaViewModelToEntity(VendaViewModel ViewModel, VendaService VendaService)
        {
            var venda = new Venda();
            var vendedor = new Vendedor();

            venda.IdPedido = Guid.NewGuid().ToString();
            venda.Data = DateTime.Now;
            venda.Status = ViewModel.Status;
            var itensToAdd = new List<Item>();

            var vendedorBanco = VendaService.BuscarVendedorPorNome(ViewModel.Vendedor.Nome);

            if (vendedorBanco.Any())
            {
                venda.Vendedor = vendedorBanco.First();
            }
            else
            {
                vendedor.Email = ViewModel.Vendedor.Email;
                vendedor.Nome = ViewModel.Vendedor.Nome;
                vendedor.Cpf = ViewModel.Vendedor.Cpf;
                vendedor.Telefone = ViewModel.Vendedor.Telefone;

                venda.Vendedor = vendedor;
            }
            if (ViewModel.Itens != null && ViewModel.Itens.Any())
            {
                foreach (var itensLista in ViewModel.Itens)
                {
                    Item item = new()
                    {
                        Descricao = itensLista.Descricao,
                        Quantidade = itensLista.Quantidade,
                        Preco = itensLista.Preco
                    };

                    itensToAdd.Add(item);
                }
                venda.Itens = itensToAdd;
            }
            else
            {
                venda.Itens = null;
            }            

            return venda;
        }
        public VendaViewModel VendaEntityToViewModel(Venda Entity, VendaService VendaService)
        {

            var vendedorBanco = VendaService.BuscarVendedorPorId(Entity.IdVendedor);

            var itensBanco = VendaService.BuscarItens(Entity.IdVenda);

            var vendaModel = new VendaViewModel();
            var vendedorModel = new VendedorViewModel();

            var itensToAdd = new List<ItemViewModel>();

            vendedorModel.Cpf = vendedorBanco.Cpf;
            vendedorModel.Nome = vendedorBanco.Nome;
            vendedorModel.Email = vendedorBanco.Email;
            vendedorModel.Telefone = vendedorBanco.Telefone;

            foreach (var itemBanco in itensBanco)
            {
                ItemViewModel itemModel = new()
                {
                    Descricao = itemBanco.Descricao,
                    Quantidade = itemBanco.Quantidade,
                    Preco = itemBanco.Preco
                };

                itensToAdd.Add(itemModel);
            }

            vendaModel.Vendedor = vendedorModel;
            vendaModel.Itens = itensToAdd;
            vendaModel.Status = Entity.Status;

            return vendaModel;
        }

    }
}

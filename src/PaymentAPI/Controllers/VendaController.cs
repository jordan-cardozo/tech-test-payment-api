﻿using Application.Interfaces;
using Application.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PaymentAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendaController : ControllerBase
    {
        private readonly IAppVenda AppVenda;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_AppVenda"></param>
        public VendaController(IAppVenda _AppVenda)
        {
            AppVenda = _AppVenda;
        }

        /// <summary>
        /// Retorna a Venda conforme o id informado
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Venda conforme o id informado.</returns>
        /// <response code="200">Venda com o id informado encontrada.</response>
        /// <response code="400">Venda com o id informado não encontrada.</response>
        // GET api/<VendaController>/5
        [HttpGet("Buscar_Venda/{id}")]
        public IActionResult Get(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    return Ok(AppVenda.ObterPorId(id));
                }
                catch (Exception ex)
                {
                    return BadRequest(new { Message = ex.Message });
                }

            }
            return BadRequest();
        }

        /// <summary>
        /// Registra a Venda com o Status, o Vendedor, os Itens informados
        /// </summary>
        /// <param name="vendaModel"></param>
        /// <returns>Venda que foi registrada pelo endpoint.</returns>
        /// <response code="200">Venda registrada com sucesso.</response>
        /// <response code="400">Corpo da requisição vazio, Vendedor ou Itens não foram informados.</response>
        // POST api/<VendaController>
        [HttpPost("Registrar_Venda")]
        public IActionResult Post([FromBody] VendaViewModel vendaModel)
        {

            if (ModelState.IsValid)
            {
                VendaViewModel vendaBanco = new VendaViewModel();

                try
                {
                    vendaBanco = AppVenda.Adicionar(vendaModel);

                }
                catch (Exception ex)
                {
                    return BadRequest(new { Message = ex.Message });
                }

                if (vendaBanco == null)
                    return NotFound();

                return Ok(vendaBanco);
            }
            return BadRequest();
        }

        /// <summary>
        /// Atualiza Status da Venda
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vendaModel"></param>
        /// <returns>Venda atualizada</returns>
        /// <response code="200">Status da venda atualizado com sucesso.</response>
        /// <response code="400">Alteração de Status Inválida.</response>
        // PUT api/<VendaController>/5
        [HttpPut("Atualizar_Venda/{id}")]
        public IActionResult Put(int id, [FromBody] VendaUpdateViewModel vendaModel)
        {
            if (ModelState.IsValid)
            {
                VendaViewModel vendaBanco = new VendaViewModel();

                try
                {
                    vendaBanco = AppVenda.Atualizar(id, vendaModel);

                }
                catch (Exception ex)
                {
                    return BadRequest(new { Message = ex.Message });
                }

                if (vendaBanco == null)
                    return NotFound();

                return Ok(vendaBanco);
            }
            return BadRequest();
        }
    }
}

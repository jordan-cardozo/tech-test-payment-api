﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Application.Models
{
    /// <summary>
    /// Modelo de Vendedor para apresentação
    /// </summary>
    public class VendedorViewModel
    {
        [DefaultValue("000.000.000-00")]
        [Required(ErrorMessage = "Informe o CPF do Vendedor.", AllowEmptyStrings = false)]
        [RegularExpression("^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$", ErrorMessage = "Informe um CPF válido.")]
        public string Cpf { get; set; }

        [Required(ErrorMessage = "Informe o nome do Vendedor.", AllowEmptyStrings = false)]
        [MaxLength(255, ErrorMessage = "Tamanho máximo de {0} caracteres.")]
        public string Nome { get; set; }

        [DefaultValue("user@email.com")]
        [Required(ErrorMessage = "Informe o E-mail do Vendedor.", AllowEmptyStrings = false)]
        [RegularExpression("^([\\w\\.\\-]+)@([\\w\\-]+)((\\.(\\w){2,3})+)$", ErrorMessage = "Informe um E-mail válido.")]
        public string Email { get; set; }

        [DefaultValue("(00)00000-0000")]
        [Required(ErrorMessage = "Informe o DDD entre parênteses mais o Telefone do Vendedor.", AllowEmptyStrings = false)]
        [RegularExpression("(\\(\\d{2}\\))(\\d{4,5}\\-\\d{4})", ErrorMessage = "Informe um Telefone válido.")]
        public string Telefone { get; set; }
    }
}

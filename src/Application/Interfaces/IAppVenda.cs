﻿using Application.Models;

namespace Application.Interfaces
{
    public interface IAppVenda
    {
        VendaViewModel Adicionar(VendaViewModel Entity);
        VendaViewModel Atualizar(int id, VendaUpdateViewModel venda);
        VendaViewModel ObterPorId(int Id);
    }
}

using Application.Adapter;
using Application.Apps;
using Application.Interfaces;
using Domain.Interfaces.Vendas;
using Domain.Services;
using Infra.Context;
using Infra.Repository.Vendas;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<DatabaseContext>(options => options.UseInMemoryDatabase("InMemoryDatabase"));
builder.Services.AddScoped<DatabaseContext, DatabaseContext>();
builder.Services.AddScoped<VendaService, VendaService>();


//Dependency Injection
builder.Services.AddMvc();
builder.Services.AddScoped<InterfaceVenda, RepositoryVendas>();
builder.Services.AddScoped<IAppVenda, ApplicationVenda>();
builder.Services.AddScoped<IAdapterVenda, VendaAdapter>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(x =>
{
    x.SwaggerDoc("v1",
        new OpenApiInfo
        {
            Title = "Api de Vendas",
            Version = "v1.0",
            Description = "Api de Vendas",
            Contact = new OpenApiContact
            {
                Name = "Desenvolvedor",
                Url = new Uri("https://github.com/jordan-cardozo"),
                Email = "jordant2ng@yahoo.com.br"
            }
        });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    x.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI(options =>
{
    options.RoutePrefix = "api-docs";
    options.SwaggerEndpoint("/swagger/v1/swagger.json", "Api de Vendas v1");
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

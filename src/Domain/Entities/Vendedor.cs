﻿namespace Domain.Entities
{
    /// <summary>
    /// Vendedor da Venda
    /// </summary>
    public class Vendedor
    {
        public int IdVendedor { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        /// <summary>
        /// Propriedades de navegação relacional
        /// </summary>
        public virtual IList<Venda> Vendas { get; set; }
    }
}

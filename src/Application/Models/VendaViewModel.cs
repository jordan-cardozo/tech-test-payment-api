﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Application.Models
{
    /// <summary>
    /// Modelo de Venda para apresentação
    /// </summary>
    public class VendaViewModel
    {
        public VendedorViewModel Vendedor { get; set; }
        public List<ItemViewModel> Itens { get; set; }

        [DefaultValue("Aguardando pagamento")]
        [Required(ErrorMessage = "Informe o status da venda.", AllowEmptyStrings = false)]
        [MinLength(8, ErrorMessage = "Tamanho mínimo de {0} caracteres.")]
        public string Status { get; set; }

    }
}

﻿namespace Domain.Entities
{
    /// <summary>
    /// Item da Venda
    /// </summary>
    public class Item
    {
        public int IdItem { get; set; }
        public string Descricao { get; set; }
        public decimal Preco { get; set; }
        public int Quantidade { get; set; }

        /// <summary>
        /// Propriedades de navegação relacional
        /// </summary>
        public virtual int IdVenda { get; set; }
        public virtual Venda Venda { get; set; }
    }
}
